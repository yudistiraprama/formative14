show databases;
create database formative14;
use formative14;

create table formative14.brand (
id int not null auto_increment,
name varchar(50),
primary key (id));

create table formative14.product (
id int not null auto_increment,
artNumber varchar(50),
name varchar(50),
description varchar(50),
brandId int,
primary key (id),
FOREIGN KEY fk_brand(brandId) REFERENCES brand(id));

insert into brand (name) values
('Nike'), ('Addidas'), ('Puma'), ('Vans'), ('Converse');

insert into product (artNumber, name, description, brandId) values
('NKE-001', 'Nike Classic', 'Canvas Premium', 1),
('NKE-002', 'Nike Air', 'Air Force Foam', 1),
('ADS-001', 'Addidas Classic', 'Canvas Premium', 2),
('ADS-002', 'Addidas Running', 'Foam Ultimate', 2),
('PMA-001', 'Puma Classic', 'Canvas Premium', 3),
('PMA-002', 'Puma Soft Foam', 'Premium Soft Foam', 3),
('VNS-001', 'Vans Oldskool', 'Canvas Premium', 4),
('VNS-002', 'Vans Checkerboard', 'Canvas Premium', 4),
('CNV-001', 'Converse All-Star Classic', 'Canvas Premium', 5),
('CNV-002', 'Converse Run', 'Premium Foan Cotton', 5);

select * from brand;
select * from product;
