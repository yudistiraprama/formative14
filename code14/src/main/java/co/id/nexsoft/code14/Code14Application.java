package co.id.nexsoft.code14;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Code14Application {

	public static void main(String[] args) {
		SpringApplication.run(Code14Application.class, args);
	}

}
