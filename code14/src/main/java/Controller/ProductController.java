package Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import Entitiy.Product;
import Repository.ProductRepository;

@RestController
public class ProductController {

	@Autowired
	private ProductRepository prodRepo;

	// Home Page
	@GetMapping("/")
	public String welcome() {
		return "<html><body>" + "<h1>WELCOME TO BRAND & PRODUCT</h1>" + "</body></html>";
	}

	// Get All Notes

	@GetMapping("/product")
	public List<Product> getAllProduct() {
		return prodRepo.findAll();
	}

	// Get the company details by // ID

	@GetMapping("/product/{id}")
	public Product getProductById(@PathVariable(value = "id") int id) {
		return prodRepo.findById(id);
	}
}
