package Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import Entitiy.Brand;
import Repository.BrandRepository;

@RestController
public class BrandController {

	@Autowired
	private BrandRepository brandRepo;

	// Get All Notes
	@GetMapping("/brand")
	public List<Brand> getAllBrand() {
		return brandRepo.findAll();
	}

	// Get the company details by
	// ID
	@GetMapping("/brand/{id}")
	public Brand getBrandById(@PathVariable(value = "id") int id) {
		return brandRepo.findById(id);
	}
}
