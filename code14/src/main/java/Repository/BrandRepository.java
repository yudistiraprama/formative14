package Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import Entitiy.Brand;

@Repository
public interface BrandRepository extends CrudRepository<Brand, Integer>{
	Brand findById(int id);
    List<Brand> findAll();
    void deleteById(int id);
}
