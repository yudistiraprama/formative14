package Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import Entitiy.Product;

@Repository
public interface ProductRepository extends CrudRepository<Product, Integer>{
	Product findById(int id);
    List<Product> findAll();
    void deleteById(int id);
}
